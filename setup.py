"""Bayesian parametric population models (pop_models)

Long description goes here...
"""

from datetime import date


#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__        = "pop_models"
__version__     = "1.0.0"
__date__        = date(2019, 1, 15)
__keywords__    = [
    "astronomy",
    "information analysis",
    "machine learning",
    "physics",
]
__status__      = "stable"


#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__         = "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models"
#__download_url__= "https://github.com/oshaughn/InferenceTools-LIGO-ScienceMode/releases/tag/{version}".format(version=__version__)
__bugtrack_url__= "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models/issues"


#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__      = "Daniel Wysocki and Richard O'Shaughnessy"
__author_email__= "daniel.wysocki@ligo.org"

__maintainer__      = "Daniel Wysocki"
__maintainer_email__= "daniel.wysocki@ligo.org"

__credits__     = ("Daniel Wysocki", "Richard O'Shaughnessy",)


#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__   = 'Copyright (c) 2017-2019 {author} <{email}>'.format(
    author=__author__,
    email=__author_email__
)

__license__     = 'MIT License'
__license_full__= '''
MIT License

{copyright}

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''.format(copyright=__copyright__).strip()


#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 5 - Production/Stable
Programming Language :: Python
Programming Language :: Python :: 2
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
Topic :: Scientific/Engineering :: Information Analysis
""".strip()

REQUIREMENTS = {
    "install": [
        "astropy>=1.3.0,<1.4.0",
        "emcee>=2.2.0,<2.3.0",
        "h5py>=2.7.0,<2.8.0",
        "mpmath>=1.0.0,<1.1.0",
        "numpy>=1.13.0,<1.14.0",
        "matplotlib>=2.0.0,<3.0.0",
        "pandas>=0.21.0,<0.22.0",
        "scipy>=1.0.0,<1.1.0",
        "seaborn>=0.8.0,<0.9.0",
        "six>=1.10.0,<1.11.0",
    ],
    "tests": [
    ]
}

ENTRYPOINTS = {
    "console_scripts" : [
        "pop_models_vt = pop_models.vt:_main",
        "pop_models_vt_plot = pop_models.vt:_main_plot",
        "pop_models_extract_samples = pop_models.sample_extraction.cli:_main",
        "pop_models_powerlaw_mcmc = pop_models.powerlaw.mcmc:_main",
        "pop_models_powerlaw_ci = pop_models.powerlaw.confidence_intervals:_main",
        "pop_models_powerlaw_marginal_plots = pop_models.powerlaw.marginal_plots:_main",
#        "pop_models_powerlaw_diagnostic_plots = pop_models.powerlaw.diagnostic_plots:_main",
#        "pop_models_powerlaw_debug_plots = pop_models.powerlaw.debug_plots:_main",
        "pop_models_powerlaw_dist_plots = pop_models.powerlaw.dist_plots:_main",
        "pop_models_powerlaw_joint_dist_plot = pop_models.powerlaw.joint_dist_plot:_main",
        "pop_models_powerlaw_spin_mag_mcmc = pop_models.powerlaw_spin_mag.mcmc:_main",
        "pop_models_powerlaw_spin_mag_marginal_plots = pop_models.powerlaw_spin_mag.marginal_plots:_main",
        "pop_models_powerlaw_spin_mag_dist_plots = pop_models.powerlaw_spin_mag.dist_plots:_main",
        "pop_models_powerlaw_spin_vec_mcmc = pop_models.powerlaw_spin_vec.mcmc:_main",
        "pop_models_powerlaw_spin_vec_marginal_plots = pop_models.powerlaw_spin_vec.marginal_plots:_main",
        "pop_models_powerlaw_spin_vec_dist_plots = pop_models.powerlaw_spin_vec.dist_plots:_main",
        "pop_models_powerlaw_spin_vec_ppd = pop_models.powerlaw_spin_vec.ppd:_main",
        "pop_models_powerlaw_spin_vec_cr = pop_models.powerlaw_spin_vec.credible_regions:_main",
        "pop_models_powerlaw_spin_vec_mass_cr = pop_models.powerlaw_spin_vec.mass_credible_regions:_main",
        "pop_models_gaussian_1D_mcmc = pop_models.gaussian_mass_1D.mcmc:_main",
        "pop_models_gaussian_1D_marginal_plots = pop_models.gaussian_mass_1D.marginal_plots:_main",
        "pop_models_gaussian_1D_dist_plots = pop_models.gaussian_mass_1D.dist_plots:_main",
        "pop_models_gaussian_1D_ppd = pop_models.gaussian_mass_1D.ppd:_main",
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name        =__name__,
    version     =__version__,
    description =DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords    =__keywords__,

    author      =__author__,
    author_email=__author_email__,

    maintainer  =__maintainer__,
    maintainer_email=__maintainer_email__,

    url         =__url__,
#    download_url=__download_url__,

    license     =__license__,

    classifiers=[f for f in CLASSIFIERS.split('\n') if f],

    package_dir ={"": "src"},
    packages    =[
        "pop_models",
        "pop_models.sample_extraction",
        "pop_models.powerlaw",
        "pop_models.powerlaw_spin_mag",
        "pop_models.powerlaw_spin_vec",
        "pop_models.gaussian_mass_1D",
    ],
#    packages=find_packages(exclude=['tests', 'tests.*']),

    install_requires=REQUIREMENTS["install"],
#    tests_require=REQUIREMENTS["tests"],

    entry_points=ENTRYPOINTS
)

setup(**metadata)
