The pop_models API
==================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 3

   pop_models.sample_extraction
   pop_models.sample_extraction.max_posterior
   pop_models.sample_extraction.integrated_acorr
   pop_models.mc
   pop_models.mcmc
   pop_models.powerlaw
   pop_models.powerlaw.confidence_intervals
   pop_models.powerlaw.dist_plots
   pop_models.powerlaw.joint_dist_plot
   pop_models.powerlaw.marginal_plots
   pop_models.powerlaw.mcmc
   pop_models.powerlaw.plot_config
   pop_models.powerlaw.prob
   pop_models.powerlaw.utils
   pop_models.powerlaw_spin_vec
   pop_models.powerlaw_spin_vec.mcmc
   pop_models.powerlaw_spin_vec.prob
   pop_models.prob
   pop_models.stats
   pop_models.utils
   pop_models.vt
