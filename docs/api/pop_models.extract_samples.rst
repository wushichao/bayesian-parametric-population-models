pop_models.extract_samples module
=================================

.. automodule:: pop_models.extract_samples
    :members:
    :undoc-members:
    :show-inheritance:
