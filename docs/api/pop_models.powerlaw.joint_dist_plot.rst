pop_models.powerlaw.joint_dist_plot module
==========================================

.. automodule:: pop_models.powerlaw.joint_dist_plot
    :members:
    :undoc-members:
    :show-inheritance:
