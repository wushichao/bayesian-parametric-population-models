pop_models.powerlaw.mcmc module
===============================

.. automodule:: pop_models.powerlaw.mcmc
    :members:
    :undoc-members:
    :show-inheritance:
