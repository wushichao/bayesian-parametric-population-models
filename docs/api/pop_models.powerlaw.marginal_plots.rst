pop_models.powerlaw.marginal_plots module
=========================================

.. automodule:: pop_models.powerlaw.marginal_plots
    :members:
    :undoc-members:
    :show-inheritance:
