Specific Models
===============

.. toctree::
   :maxdepth: 2

   powerlaw
   powerlaw_spin_mag
   powerlaw_spin_vec
   gaussian_1d
