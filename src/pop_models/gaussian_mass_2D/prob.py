r"""
This module defines all of the probability density functions and random sampling
functions for the power law mass distribution model.
"""

from __future__ import division, print_function

# Names of all (possibly free) parameters of this model, in the order they
# should appear in any array of samples (e.g., MCMC posterior samples).
param_names = [
    "log10_rate",
    "meanA", "meanB",
    "varA", "varB", "thetaAB",
]
# Number of (possibly free) parameters for this population model.
ndim_pop = len(param_names)


def Mu_Sigma_from_params(pop_params):
    import numpy

    log10_rate, meanA, meanB, varA, varB, thetaAB = pop_params
    costhetaAB = numpy.cos(thetaAB)
    sinthetaAB = numpy.sin(thetaAB)

    Rot = numpy.array([
        [costhetaAB, -sinthetaAB],
        [sinthetaAB,  costhetaAB],
    ])

    # Compute R M
    Mu = numpy.array([mean1, mean2])
    numpy.dot(Rot, Mu, out=Mu)

    # Compute R S R^T
    Sigma = numpy.diag([varA, varB])
    numpy.dot(Rot, Sigma, out=Sigma)
    numpy.dot(Signa, Rot.T, out=Sigma)

    return Mu, Sigma


def gauss2d(Mu, Sigma):
    import scipy.stats

    return scipy.stats.multivariate_normal(
        mean=Mu, cov=Sigma,
        allow_singular=True,
    )


def joint_rvs(N, pop_params, rand_state=None):
    import scipy.stats
    from ..prob import sample_with_cond

    Mu, Sigma = Mu_Sigma_from_params(pop_params)
    Normal = gauss2d(Mu, Sigma)

    def cond(m1_m2):
        """
        Given an array, where each row contains a pair ``(m_1, m_2)``, returns
        an array whose value is ``True`` when ``m_1 + m_2 <= M_max`` and
        ``False`` otherwise.
        """
        m1, m2 = m1_m2.T
        return m1 >= m2

    return sample_with_cond(Normal.rvs, shape=N, cond=cond)


def joint_pdf(indiv_params, pop_params):
    import numpy

    m_1, m_2 = indiv_params
    Mu, Sigma = Mu_Sigma_from_params(pop_params)
    Normal = gauss2d(Mu, Sigma)

    p = numpy.zeros_like(m_1)

    idx = m_1 >= m_2
    p[idx] = Normal.pdf(numpy.column_stack((m_1[idx], m_2[idx])))

    return p
