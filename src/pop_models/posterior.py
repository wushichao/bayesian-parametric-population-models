from __future__ import division, print_function

def log_posterior(
        variables,
        param_names, constants, duplicates,
        intensity_fn, expval_fn, log_prior_fn,
        event_posterior_samples, event_posterior_sample_priors,
        before_prior_aux_fn, after_prior_aux_fn,
        args, kwargs,
    ):
    import numpy

    # Determine array of parameters by putting together variables, constants,
    # and duplicates.
    params = get_params(variables, constants, duplicates, param_names)

    # Compute auxiliary information, if requested.
    if before_prior_aux_fn is not None:
        aux_info = before_prior_aux_fn(params, *args, **kwargs)
    else:
        aux_info = None

    # Compute the log of the prior.  We will call this the log of the posterior,
    # and incrementally add other contributions to it.
    log_post = log_prior_fn(params, aux_info, *args, **kwargs)

    prior_support = numpy.isfinite(log_post)

    if after_prior_aux_fn is not None:
        aux_info = after_prior_aux_fn(
            params, prior_support, aux_info, *args, **kwargs
        )

    # Add the contribution from each event to the posterior.
    iterables = zip(event_posterior_samples, event_posterior_sample_priors)
    for samples, priors in iterables:
        intensity = intensity_fn(
            samples, params, prior_support, aux_info, *args, **kwargs
        )

        if priors is not None:
            intensity /= priors

        log_post[prior_support] += numpy.log(
            numpy.mean(intensity, axis=-1)[prior_support]
        )

    # Add the contribution from the Poisson average, -mu.
    log_post[prior_support] -= expval_fn(
        params, prior_support, aux_info,
        *args, **kwargs
    )[prior_support]

    # Return log(post) + const = log(prior) + log(event contribution) - mu
    return log_post


def get_params(variables, constants, duplicates, names):
    import numpy
    import six

    input_shape = numpy.shape(variables)
    shape = input_shape[:-1]

    n_variables = input_shape[-1]
    n_constants = len(constants)
    n_duplicates = len(duplicates)
    n_params = len(names)

    if n_variables + n_constants + n_duplicates != n_params:
        raise ValueError(
            "Incorrect number of variables and constants. "
            "Expected {expected}, but got {actual}."
            .format(
                expected=n_params,
                actual=n_variables+n_constants+n_duplicates,
            )
        )

    params = numpy.empty(shape + (n_params,), dtype=numpy.float64)
    dup_indices = {}
    i = j = 0

    for name in names:
        # Use the constant value.
        if name in constants:
            param = constants[name]
        # Leave this for now, will fill duplicates at the end.
        elif name in duplicates:
            j += 1
            continue
        # Use the variable value.
        else:
            param = variables[...,i]
            i += 1

        # Store the chosen parameter.
        params[...,j] = param
        j += 1

    # Copy the duplicates into their respective indices.
    for target, source in six.iteritems(duplicates):
        params[..., names.index(target)] = params[..., names.index(source)]

    return params
