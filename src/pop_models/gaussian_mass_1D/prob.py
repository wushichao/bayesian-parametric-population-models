r"""
This module defines all of the probability density functions and random sampling
functions for the power law mass distribution model.
"""

from __future__ import division, print_function, unicode_literals

# Names of all (possibly free) parameters of this model, in the order they
# should appear in any array of samples (e.g., MCMC posterior samples).
param_names_base = [
    "log10_rate",
    "mu_m", "log10_sigma_m",
    "m_min", "m_max",
]
param_names_spin = [
    "mu_chi", "sigma_sq_chi", "chi_min", "chi_max",
]
param_names_full = param_names_base + param_names_spin

def param_names(spins=False):
    """
    Names of all (possibly free) parameters of this model, in the order they
    should appear in any array of samples (e.g., MCMC posterior samples).

    Spin parameters are only included when ``spins=True`` is set.
    """
    return param_names_full if spins else param_names_base


def ndim_pop(spins=False):
    """
    Number of (possibly free) parameters for this population model.
    """
    return len(param_names(spins=spins))


def get_base_pop_params(pop_params):
    return pop_params[:len(param_names_base)]


def get_spin_pop_params(pop_params):
    return pop_params[len(param_names_base):]



def spin_mu_sigma_sq_2_alpha_beta(mu, sigma_sq):
    import numpy

    mu_sq = numpy.square(mu)

    alpha = (mu_sq - mu*mu_sq - mu*sigma_sq) / sigma_sq
    beta = (mu-1) * (sigma_sq + mu_sq - mu) / sigma_sq

    return alpha, beta


def spin_alpha_beta_2_mu_sigma_sq(alpha, beta):
    import numpy

    a_plus_b = alpha + beta

    mu = alpha / a_plus_b
    sigma_sq = alpha*beta / (numpy.square(a_plus_b) * (a_plus_b + 1))

    return mu, sigma_sq


def get_masses(indiv_params):
    return indiv_params.T[:2]


def get_spins(indiv_params):
    return indiv_params.T[2:4]


def truncnorm(mu, sigma, lower, upper):
    import scipy.stats
    a, b = (lower-mu)/sigma, (upper-mu)/sigma
    return scipy.stats.truncnorm(a, b, loc=mu, scale=sigma)


def truncnorm2d_rvs(N, mu, sigma, lower, upper):
    import numpy

    Norm = truncnorm(mu, sigma, lower, upper)
    return numpy.column_stack((Norm.rvs(N), Norm.rvs(N)))


def truncnorm2d_pdf(x, y, mu, sigma, lower, upper):
    import numpy

    Norm = truncnorm(mu, sigma, lower, upper)
    return Norm.pdf(x) * Norm.pdf(y)


def truncbeta(alpha, beta, lower, upper):
    import scipy.stats
    return scipy.stats.beta(alpha, beta, loc=lower, scale=upper-lower)


def truncbeta2d_rvs(N, alpha, beta, lower, upper):
    import numpy

    Beta = truncbeta(alpha, beta, lower, upper)
    return numpy.column_stack((Beta.rvs(N), Beta.rvs(N)))


def truncbeta2d_pdf(x, y, alpha, beta, lower, upper):
    import numpy

    Beta = truncbeta(alpha, beta, lower, upper)
    return Beta.pdf(x) * Beta.pdf(y)


def joint_rvs(N, pop_params, spins=False, rand_state=None):
    import numpy
    import scipy.stats
    from ..prob import sample_with_cond

    log10_rate, mu_m, log10_sigma_m, m_min, m_max = (
        get_base_pop_params(pop_params)
    )
    sigma_m = numpy.power(10.0, log10_sigma_m)

    def rvs_mass(N):
        return truncnorm2d_rvs(N, mu_m, sigma_m, m_min, m_max)

    def cond_mass(m1_m2):
        """
        Given an array, where each row contains a pair ``(m_1, m_2)``, returns
        an array whose value is ``True`` when ``m_1 >= m_2`` and ``False``
        otherwise.
        """
        m1, m2 = m1_m2.T
        return m1 >= m2

    mass_samples = sample_with_cond(rvs_mass, shape=N, cond=cond_mass)

    if spins:
        mu_chi, sigma_sq_chi, chi_min, chi_max = get_spin_pop_params(pop_params)
        alpha, beta = spin_mu_sigma_sq_2_alpha_beta(mu_chi, sigma_sq_chi)

        spin_samples = truncbeta2d_rvs(N, alpha, beta, chi_min, chi_max)
        return numpy.column_stack((mass_samples, spin_samples))
    else:
        return mass_samples


def joint_pdf(indiv_params, pop_params, spins=False):
    import numpy

    log10_rate, mu_m, log10_sigma_m, m_min, m_max = (
        get_base_pop_params(pop_params)
    )
    sigma_m = numpy.power(10.0, log10_sigma_m)
    m_1, m_2 = get_masses(indiv_params)

    p = numpy.zeros_like(m_1)

    idx = m_1 >= m_2
    p[idx] = (
        2 * truncnorm2d_pdf(m_1[idx], m_2[idx], mu_m, sigma_m, m_min, m_max)
    )

    if spins:
        mu_chi, sigma_sq_chi, chi_min, chi_max = get_spin_pop_params(pop_params)
        alpha, beta = spin_mu_sigma_sq_2_alpha_beta(mu_chi, sigma_sq_chi)

        chi_1z, chi_2z = get_spins(indiv_params)

        p[idx] *= truncbeta2d_pdf(
            chi_1z[idx], chi_2z[idx],
            alpha, beta, chi_min, chi_max,
        )

    return p


def marginal_mass_pdf(m, mu_m, log10_sigma_m, m_min, m_max):
    import numpy
    sigma_m = numpy.power(10.0, log10_sigma_m)
    Norm = truncnorm(mu_m, sigma_m, m_min, m_max)
    return Norm.pdf(m)


def marginal_spin_pdf(chi_z, mu_chi, sigma_sq_chi, chi_min, chi_max):
    alpha, beta = spin_mu_sigma_sq_2_alpha_beta(mu_chi, sigma_sq_chi)

    Beta = truncbeta(alpha, beta, chi_min, chi_max)
    return Beta.pdf(chi_z)
