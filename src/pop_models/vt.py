"""
This module is a slightly modified version of a module written by Will Farr,
available at
<https://git.ligo.org/RatesAndPopulations/O2Populations/blob/fe81c5d064283c94e12a19567e020b9e9930efef/code/vt.py>

The main changes made here are for data I/O, and the majority of the
implementation is the original code written by Will Farr.
"""


from __future__ import print_function



import astropy.cosmology as cosmo
import astropy.units as u
import multiprocessing as multi
import numpy as np
from numpy import (
    array, linspace,
    sin, cos,
    square,
    trapz,
)

#from pylab import *

def draw_thetas(N):
    """Draw `N` random angular factors for the SNR.

    Theta is as defined in [Finn & Chernoff
    (1993)](https://ui.adsabs.harvard.edu/#abs/1993PhRvD..47.2198F/abstract).
    """

    cos_thetas = np.random.uniform(low=-1, high=1, size=N)
    cos_incs = np.random.uniform(low=-1, high=1, size=N)
    phis = np.random.uniform(low=0, high=2*np.pi, size=N)
    zetas = np.random.uniform(low=0, high=2*np.pi, size=N)

    Fps = 0.5*cos(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) - sin(2*zetas)*cos_thetas*sin(2*phis)
    Fxs = 0.5*sin(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) + cos(2*zetas)*cos_thetas*sin(2*phis)

    return np.sqrt(0.25*square(Fps)*square(1 + square(cos_incs)) + square(Fxs)*square(cos_incs))



def next_pow_two(x):
    """Return the next (integer) power of two above `x`.

    """

    x2 = 1
    while x2 < x:
        x2 = x2 << 1
    return x2

def optimal_snr(
        m1_intrinsic, m2_intrinsic, s1z, s2z, z,
        fmin=19.0,
        psd_fn=None,
    ):
    """Return the optimal SNR of a signal.

    :param m1_intrinsic: The source-frame mass 1.

    :param m2_intrinsic: The source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param z: The redshift.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: A function that returns the detector PSD at a given
      frequency (default is early aLIGO high sensitivity, defined in
      [P1200087](https://dcc.ligo.org/LIGO-P1200087/public).

    :return: The SNR of a face-on, overhead source.

    """
    import lal
    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    # Get dL, Gpc
    dL = cosmo.Planck15.luminosity_distance(z).to(u.Gpc).value

    # Basic setup: min frequency for w.f., PSD start freq, etc.
#    fmin = 19.0
    fref = 40.0
    psdstart = 20.0

    # NOTE: Added s1z and s2z for aligned spin VT
    # This is a conservative estimate of the chirp time + MR time (2 seconds)
    tmax = ls.SimInspiralChirpTimeBound(fmin, m1_intrinsic*(1+z)*lal.MSUN_SI, m2_intrinsic*(1+z)*lal.MSUN_SI, s1z, s2z) + 2

    df = 1.0/next_pow_two(tmax)
    fmax = 2048.0 # Hz --- based on max freq of 5-5 inspiral

    # NOTE: Added s1z and s2z for aligned spin VT
    # Generate the waveform, redshifted as we would see it in the detector, but with zero angles (i.e. phase = 0, inclination = 0)
## Will's version -- apparently from a different version of lalsim
##    hp, hc = ls.SimInspiralChooseFDWaveform((1+z)*m1_intrinsic*lal.MSUN_SI, (1+z)*m2_intrinsic*lal.MSUN_SI, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, dL*1e9*lal.PC_SI, 0.0, 0.0, 0.0, 0.0, 0.0, df, fmin, fmax, fref, None, ls.IMRPhenomPv2)
    hp, hc = ls.SimInspiralChooseFDWaveform(0.0, df, (1+z)*m1_intrinsic*lal.MSUN_SI, (1+z)*m2_intrinsic*lal.MSUN_SI, 0.0, 0.0, s1z, 0.0, 0.0, s2z, fmin, fmax, fref, dL*1e9*lal.PC_SI, 0.0, 0.0, 0.0, None, None, 0, 0, ls.IMRPhenomPv2)

    Nf = int(round(fmax/df)) + 1
    fs = linspace(0, fmax, Nf)
    sel = fs > psdstart

    # PSD
    sffs = lal.CreateREAL8FrequencySeries("psds", 0, 0.0, df, lal.DimensionlessUnit, fs.shape[0])
    psd_fn(sffs, psdstart)

    return ls.MeasureSNRFD(hp, sffs, psdstart, -1.0)


# Variable only used by ``fraction_above_threshold``. The value is constant, and
# needs to only be computed once. It also takes a non-negligible amount of time
# to compute, and so it is computed the first time ``fraction_above_threshold``
# is called, as one might import this module without calling that function.
# NOTE: This is a change from Will Farr's original code.
_thetas = None

def fraction_above_threshold(m1_intrinsic, m2_intrinsic, s1z, s2z, z, snr_thresh, fmin=19.0, psd_fn=None):
    """Returns the fraction of sources above a given threshold.

    :param m1_intrinsic: Source-frame mass 1.

    :param m2_intrinsic: Source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param z: Redshift.

    :param snr_thresh: SNR threshold.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: Function computing the PSD (see :func:`optimal_snr`).

    :return: The fraction of sources that are above the given
      threshold.

    """
    global _thetas

    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    # Compute ``_thetas`` once and for all. It is stored as a global variable,
    # but it should also not be used outside of this function.
    if _thetas is None:
        _thetas = draw_thetas(10000)

    if z == 0.0:
        return 1.0

    rho_max = optimal_snr(
        m1_intrinsic, m2_intrinsic, s1z, s2z, z,
        fmin=fmin,
        psd_fn=psd_fn,
    )

    # From Finn & Chernoff, we have SNR ~ theta*integrand, assuming that the polarisations are
    # orthogonal
    theta_min = snr_thresh / rho_max

    if theta_min > 1:
        return 0.0
    else:
        return np.mean(_thetas > theta_min)

def vt_from_mass_spin(
        m1, m2, s1z, s2z,
        thresh,
        analysis_time,
        calfactor=1.0,
        fmin=19.0,
        psd_fn=None,
    ):
    """Returns the sensitive time-volume for a given system.

    :param m1: Source-frame mass 1.

    :param m2: Source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param analysis_time: The total detector-frame searched time.

    :param calfactor: Fudge factor applied multiplicatively to the final result.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: Function giving the assumed single-detector PSD
      (see :func:`optimal_snr`).

    :return: The sensitive time-volume in comoving Gpc^3-yr (assuming
      analysis_time is given in years).

    """
    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    def integrand(z):
        if z == 0.0:
            return 0.0
        else:
            return (
                4*np.pi *
                cosmo.Planck15.differential_comoving_volume(z)
                  .to(u.Gpc**3 / u.sr).value /
                (1+z) *
                fraction_above_threshold(
                    m1, m2, s1z, s2z, z,
                    thresh,
                    fmin=fmin,
                    psd_fn=psd_fn,
                )
            )

    # NOTE: Extended zmax for aligned spin, as there were detections at z=1.0
#    zmax = 1.0
    zmax = 1.5
    zmin = 0.001
    assert fraction_above_threshold(
          m1, m2, s1z, s2z, zmax,
          thresh,
          fmin=fmin, psd_fn=psd_fn,
      ) == 0.0
    assert fraction_above_threshold(
        m1, m2, s1z, s2z, zmin,
        thresh,
        fmin=fmin, psd_fn=psd_fn,
    ) > 0.0
    while zmax - zmin > 1e-3:
        zhalf = 0.5*(zmax+zmin)
        fhalf = fraction_above_threshold(
            m1, m2, s1z, s2z, zhalf,
            thresh,
            fmin=fmin,
            psd_fn=psd_fn,
        )

        if fhalf > 0.0:
            zmin=zhalf
        else:
            zmax=zhalf

    zs = linspace(0.0, zmax, 20)
    ys = array([integrand(z) for z in zs])
    return calfactor*analysis_time*trapz(ys, zs)

class VTFromMassSpinTuple(object):
    def __init__(self, thresh, analyt, calfactor, psd_fn, fmin=19.0):
        self.thresh = thresh
        self.analyt = analyt
        self.calfactor = calfactor
        self.psd_fn = psd_fn
        self.fmin = fmin
    def __call__(self, m1m2s1zs2z):
        m1, m2, s1z, s2z = m1m2s1zs2z
        return vt_from_mass_spin(
            m1, m2, s1z, s2z,
            self.thresh, self.analyt,
            calfactor=self.calfactor,
            fmin=self.fmin,
            psd_fn=self.psd_fn,
        )

def vts_from_masses_spins(
        m1s, m2s, s1zs, s2zs, thresh, analysis_time,
        calfactor=1.0,
        fmin=19.0,
        psd_fn=None,
        processes=None,
        random_state=None,
    ):
    """Returns array of VTs corresponding to the given systems.

    Uses multiprocessing for more efficient computation.
    """
    import numpy
    import lalsimulation as ls

    if random_state is None:
        random_state = numpy.random.RandomState()

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    vt_ms_tuple = VTFromMassSpinTuple(
        thresh, analysis_time, calfactor, psd_fn,
        fmin=fmin,
    )

    # Re-order the samples randomly, as a rudimentary form of load balancing.
    # If we assign the same thread only samples that are close in parameter
    # space, some threads will be overburdened with parameters that take a long
    # time to evaluate (i.e., low mass). Randomizing can't hurt, and will tend
    # to improve performance significantly.
    n = len(m1s)
    i = random_state.choice(n, n, replace=False)

    vts = numpy.empty(n, dtype=numpy.float64)

    pool = multi.Pool(processes=processes)

    try:
        vts[i] = pool.map(vt_ms_tuple, zip(m1s[i], m2s[i], s1zs[i], s2zs[i]))
    finally:
        pool.close()

    return vts


class RegularGridVTInterpolator(object):
    def __init__(self, hdf5_file, scale_factor=None):
        import numpy
        from scipy.interpolate import RegularGridInterpolator

        self.scale_factor = scale_factor

        self.mode = hdf5_file.attrs["mode"]
        self.m_min = hdf5_file.attrs["m_min"]
        self.m_max = hdf5_file.attrs["m_max"]
        self.M_max = hdf5_file.attrs["M_max"]

        if self.mode == "zero spin":
            self.__zero_spin_init(hdf5_file)
        elif self.mode == "aligned spin":
            self.__aligned_spin_init(hdf5_file)
        else:
            raise NotImplementedError(
                "Unknown mode: {mode}".format(mode=self.mode)
            )

        VT_values = hdf5_file["VT"].value
        if scale_factor is None:
            self.__log_VT = numpy.log(VT_values)
        else:
            self.__log_VT = numpy.log(scale_factor * VT_values)

        self.__interpolator = RegularGridInterpolator(
            self.__points, self.__log_VT,
            method="linear", bounds_error=False, fill_value=-numpy.inf,
        )


    def __call__(self, *args, **kwargs):
        import numpy

        if self.mode == "zero spin":
            eval_points = self.__zero_spin_eval_points(*args, **kwargs)
        elif self.mode == "aligned spin":
            eval_points = self.__aligned_spin_eval_points(*args, **kwargs)
        else:
            raise NotImplementedError

        return numpy.exp(self.__interpolator(eval_points))



    def __zero_spin_init(self, hdf5_file):
        logM = hdf5_file["logM"].value
        qtilde = hdf5_file["qtilde"].value

        self.__points = logM, qtilde


    def __aligned_spin_init(self, hdf5_file):
        logM = hdf5_file["logM"].value
        qtilde = hdf5_file["qtilde"].value
        s1z = hdf5_file["s1z"].value
        s2z = hdf5_file["s2z"].value

        self.__points = logM, qtilde, s1z, s2z


    def __zero_spin_eval_points(self, m1, m2):
        import numpy

        logM, qtilde = mass_grid_coords(m1, m2, self.m_min)
        return numpy.column_stack((logM, qtilde))


    def __aligned_spin_eval_points(self, m1, m2, s1z, s2z):
        import numpy

        logM, qtilde = mass_grid_coords(m1, m2, self.m_min)
        return numpy.column_stack((logM, qtilde, s1z, s2z))


class CorrectedVT(object):
    def __init__(self, VT, coeffs, basis_fns):
        self.VT = VT
        self.coeffs = coeffs
        self.basis_fns = basis_fns
        self.mode = VT.mode

    def __call__(self, *args, **kwargs):
        correction_factor = sum(
            c * f(*args, **kwargs)
            for c, f in zip(self.coeffs, self.basis_fns)
        )
        return self.VT(*args, **kwargs) * correction_factor


_correction_basis_scalar_zero_spin = [
    lambda m1, m2: 1.0,
]
_correction_basis_scalar_aligned_spin = [
    lambda m1, m2, a1z, a2z: 1.0,
]

_correction_basis_linear_zero_spin = (
    _correction_basis_scalar_zero_spin +
    [
        lambda m1, m2: m1,
        lambda m1, m2: m2,
    ]
)
_correction_basis_linear_aligned_spin = (
    _correction_basis_scalar_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: m1,
        lambda m1, m2, a1z, a2z: m2,
    ]
)

_correction_basis_quadratic_zero_spin = (
    _correction_basis_linear_zero_spin +
    [
        lambda m1, m2: m1*m2,
        lambda m1, m2: m1**2,
        lambda m1, m2: m2**2,
    ]
)
_correction_basis_quadratic_aligned_spin = (
    _correction_basis_linear_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: m1*m2,
        lambda m1, m2, a1z, a2z: m1**2,
        lambda m1, m2, a1z, a2z: m2**2,
    ]
)

_correction_bases_zero_spin = {
    "scalar" : _correction_basis_scalar_zero_spin,
    "linear" : _correction_basis_linear_zero_spin,
    "quadratic" : _correction_basis_quadratic_zero_spin,
}
_correction_bases_aligned_spin = {
    "scalar" : _correction_basis_scalar_aligned_spin,
    "linear" : _correction_basis_linear_aligned_spin,
    "quadratic" : _correction_basis_quadratic_aligned_spin,
}

correction_bases = {
    "zero spin" : _correction_bases_zero_spin,
    "aligned spin" : _correction_bases_aligned_spin,
}



def mass_grid_coords(m1, m2, m_min, eps=1e-8):
    import numpy

    m1, m2 = numpy.asarray(m1), numpy.asarray(m2)

    M = m1 + m2

    logM = numpy.log(M)

    # Note: when computing qtilde, there is a coordinate singularity at
    # m1 = m2 = m_min.  While normally, equal mass corresponds to qtilde = 1,
    # here the most extreme mass ratio is also equal mass, so we should have
    # qtilde = 0.  Here we break the singularity by just forcing qtilde = 1 in
    # these cases.  We also allow for some numerical wiggle room beyond that
    # point, by also including any points within some small epsilon of the
    # singularity.  VT does not change on a fast enough scale for this to make
    # any measurable difference.
    i_good = M > 2*m_min + eps

    m1, m2, M = m1[i_good], m2[i_good], M[i_good]

    qtilde = numpy.ones_like(logM)
    qtilde[i_good] = M * (m2 - m_min) / (m1 * (M - 2*m_min))

    return logM, qtilde


def mass_grid_coords_inverse(logM, qtilde, m_min):
    import numpy

    M = numpy.exp(logM)

    m1 = M * (M - m_min) / (M * (1+qtilde) - 2*m_min*qtilde)
    m2 = (
        (M * (M*qtilde + m_min*(1-2*qtilde)))
        /
        (M*(1+qtilde) - 2*m_min*qtilde)
    )

    return m1, m2


def _get_args(raw_args):
    """
    Parse command line arguments when run in CLI mode.
    """
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("m_min", type=float)
    parser.add_argument("m_max", type=float)
    parser.add_argument("duration", type=float,
                        help="Duration of analyzed data in days")
    parser.add_argument("output")

    parser.add_argument("--log-total-mass-samples", type=int, default=50)
    parser.add_argument("--mass-ratio-samples", type=int, default=15)
    parser.add_argument("--spin-samples", type=int, default=31)

    parser.add_argument("--threshold", type=float, default=8.0)
    parser.add_argument("--calfactor", type=float, default=1.0)
    parser.add_argument("--psd-fn",
        default="SimNoisePSDaLIGOEarlyHighSensitivityP1200087"
    )

    parser.add_argument("--days-per-year", type=float, default=365.25)

    parser.add_argument("--f-min", type=float, default=19.0)

    parser.add_argument(
        "--zero-spin",
        action="store_true",
        help="Assume zero spin.",
    )


    parser.add_argument(
        "--total-mass-max",
        type=float,
        help="Stop computing VT's past this value of m1+m2.",
    )

    parser.add_argument(
        "--n-threads",
        type=int,
        help="Number of threads to use to compute VT. "
             "Uses all available threads if not specified.",
    )
    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generation.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import numpy
    import h5py
    import lalsimulation as ls

    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    random_state = numpy.random.RandomState(args.seed)

    # Load PSD function from lalsimulation, raising an exception if it
    # doesn't exist.
    try:
        psd_fn = getattr(ls, args.psd_fn)
    except AttributeError as err:
        err.message = (
            "PSD '{}' not found in lalsimulation.".format(args.psd_fn)
        )
        raise

    duration = args.duration / args.days_per_year

    M_min = 2*args.m_min
    M_max = 2*args.m_max if args.total_mass_max is None else args.total_mass_max

    log_M_min, log_M_max = numpy.log([M_min, M_max])


    with h5py.File(args.output, "w-") as f:
        logMs = numpy.linspace(
            log_M_min, log_M_max,
            args.log_total_mass_samples,
        )
        qtildes = numpy.linspace(0.0, 1.0, args.mass_ratio_samples)

        if args.zero_spin:
            LOGM, QTILDE = numpy.meshgrid(logMs, qtildes, indexing="ij")
            M1, M2 = mass_grid_coords_inverse(LOGM, QTILDE, args.m_min)
            s1z = numpy.zeros_like(M1).ravel()
            s2z = s1z
        else:
            spins = numpy.linspace(-1.0, 1.0, args.spin_samples)
            LOGM, QTILDE, S1Z, S2Z = (
                numpy.meshgrid(logMs, qtildes, spins, spins, indexing="ij")
            )
            M1, M2 = mass_grid_coords_inverse(LOGM, QTILDE, args.m_min)
            s1z, s2z = S1Z.ravel(), S2Z.ravel()
            m1, m2 = M1.ravel(), M2.ravel()

        m1, m2 = M1.ravel(), M2.ravel()

        VTs = vts_from_masses_spins(
            m1, m2, s1z, s2z,
            args.threshold, duration,
            calfactor=args.calfactor, fmin=args.f_min, psd_fn=psd_fn,
            processes=args.n_threads,
            random_state=random_state,
        ).reshape(M1.shape)

        f.create_dataset("logM", data=logMs)
        f.create_dataset("qtilde", data=qtildes)

        if not args.zero_spin:
            f.create_dataset("s1z", data=spins)
            f.create_dataset("s2z", data=spins)

            f.attrs["mode"] = "aligned spin"
        else:
            f.attrs["mode"] = "zero spin"

        f.create_dataset("VT", data=VTs)

        f.attrs["m_min"] = args.m_min
        f.attrs["m_max"] = args.m_max
        f.attrs["M_max"] = M_max
