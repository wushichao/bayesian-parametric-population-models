from __future__ import division

def cr(quantiles, samples, weights=None):
    import numpy
    import scipy.interpolate

    samples = numpy.asarray(samples)

    assert samples.ndim == 1

    i_sort = numpy.argsort(samples)

    samples = samples[i_sort]

    if weights is None:
        n_samples = len(samples)
        weights = numpy.ones(n_samples, dtype=numpy.float64) / n_samples
    else:
        weights = weights[i_sort]

    cumweights = numpy.cumsum(weights)
    cumweights /= cumweights[-1]

    interp_value = scipy.interpolate.interp1d(
        cumweights, samples,
        kind="linear",
    )

    return [interp_value(q) for q in quantiles]
