from __future__ import print_function


def arg_burned_in(log_prob, scale_factor=0.5):
    import numpy

    if scale_factor <= 0.0 or scale_factor >= 1.0:
        raise ValueError("Scale factor must be in interval (0, 1).")

    log_prob_max = numpy.max(log_prob)

    # log(scale_factor * prob) = log(scale_factor) + log(prob)
    log_scale_factor = numpy.log(scale_factor)
    log_threshold = log_scale_factor + log_prob_max

    within_threshold = log_prob >= log_threshold

    first_arg_all = numpy.argmax(within_threshold, axis=0)
    first_arg_last = numpy.max(first_arg_all)

    return first_arg_last


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing posterior samples.",
    )

    parser.add_argument(
        "--scale-factor",
        type=float, default=0.5,
        help="Fraction of max probability sample to use as cutoff.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import numpy
    import h5py

    with h5py.File(args.posteriors, "r") as posteriors:
        log_prob = posteriors["log_prob"][:]

        print(arg_burned_in(log_prob, args.scale_factor))
