from __future__ import print_function

def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "events",
        help="HDF5 file containing event likelihoods or posteriors.",
    )
    parser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )

    parser.add_argument("output_plot_likelihood")
    parser.add_argument("output_plot_intensity")
    parser.add_argument("output_plot_integrand")
    parser.add_argument("output_plot_mean_vs_alpha")

    parser.add_argument("m_min", type=float)
    parser.add_argument("m_max", type=float)

    parser.add_argument("alpha_min", type=float)
    parser.add_argument("alpha_max", type=float)

    parser.add_argument("--alpha-fixed", type=float, default=-4.0)
    parser.add_argument(
        "--alpha-list",
        type=float, nargs="+",
        default=[-4.0, -1.0, 0.0, +1.0, +2.5, +4.0],
    )

    parser.add_argument("--alpha-n-samples", default=100, type=int)
    parser.add_argument("--mass-n-samples", default=100, type=int)

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )


    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import six
    import operator
    import numpy
    import h5py
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    from .. import gw
    from .. import vt
    from . import mcmc
    from . import prob


    M_max = args.m_min + args.m_max

    with h5py.File(args.VTs, "r") as VTs:
        raw_interpolator = vt.interpolate_hdf5(VTs)

        def VT_interp(m1_m2):
            m1 = m1_m2[:,0]
            m2 = m1_m2[:,1]

            return raw_interpolator(m1, m2)

    fig_likelihood, ax_likelihood = plt.subplots()
    fig_intensity, ax_intensity = plt.subplots()
    fig_integrand, ax_integrand = plt.subplots()
    fig_mean_vs_alpha, ax_mean_vs_alpha = plt.subplots()


    m_linear = numpy.linspace(args.m_min, args.m_max, args.mass_n_samples)
    m1_mesh, m2_mesh = numpy.meshgrid(m_linear, m_linear)
    m1, m2 = m1_mesh.ravel(), m2_mesh.ravel()
    m1_m2 = numpy.column_stack((m1, m2))

#    dm = m1[1] - m1[0]
    dm = m_linear[1] - m_linear[0]

    def after_prior_aux_fn(data, indiv_params, pop_params, aux_info):
        from . import prob

        log_rate, alpha = pop_params

        norm = dm*dm * numpy.sum(
            prob.joint_pdf(
                m1, m2, alpha, args.m_min, M_max,
            )
        )

        return {"norm": norm}


    # Use fiducial rate
    rate = 1.0
    log10_rate = 0.0

    alphas = numpy.linspace(
        args.alpha_min, args.alpha_max,
        args.alpha_n_samples,
    )

    means = numpy.fromiter(
        (
            mcmc.expval_mc(
                (log10_rate, alpha),
                None,
                efficiency_fn=VT_interp,
                m_min=args.m_min, M_max=M_max,
                return_err=False,
            )
            for alpha in alphas
        ),
        dtype=numpy.float64,
    )


    VT = VT_interp(m1_m2)

    idx_outside = reduce(
        operator.__or__,
        [
            m1 <= args.m_min,
            m2 < args.m_min,
            m1 > args.m_max,
            m2 > m1,
            m1+m2 > M_max,
        ]
    )
    idx_inside = ~idx_outside

    VT[idx_outside] = 0.0
    VT = VT.reshape(m1_mesh.shape)


    cdict = {
        "red": (
            (0.0, 1.0, 1.0),
            (1.0, 0.0, 0.0),
        ),
        "green": (
            (0.0, 1.0, 1.0),
            (1.0, 0.0, 0.0),
        ),
        "blue": (
            (0.0, 1.0, 1.0),
            (1.0, 0.0, 0.0),
        ),
        "alpha": (
            (0.0, 0.0, 0.0),
            (1.0, 1.0, 1.0),
        ),
    }
    cmap = matplotlib.colors.LinearSegmentedColormap(
        "BlackWhiteTransparent", cdict,
    )

    with h5py.File(args.events, "r") as events:
        for event_name, event_group in six.iteritems(events):
            lnlike = numpy.zeros_like(m1)

            event_mean = event_group["mean"][:2]
            event_cov = event_group["cov"][:2,:2]

            indiv_data = (event_mean, event_cov)

            for i, (m1_i, m2_i) in enumerate(m1_m2):
                indiv_params = (m1_i, m2_i)
                lnlike[i] += mcmc.log_data_likelihood(
                    indiv_data, indiv_params, None,
                )

            max_lnL = numpy.max(lnlike)
            lnlike[lnlike < max_lnL - 15.0] = numpy.nan
            lnlike[idx_outside] = numpy.nan

            ax_likelihood.contourf(
                m1_mesh, m2_mesh,
                lnlike.reshape(m1_mesh.shape),
                100, cmap=cmap,
            )


    for alpha in args.alpha_list:
        pop_params = (log10_rate, alpha)
        indiv_params = (m1, m2)

        aux_info = after_prior_aux_fn(None, indiv_params, pop_params, None)

        intensity = numpy.exp(
            mcmc.log_intensity(
                indiv_params, pop_params,
                aux_info,
                m_min=args.m_min, M_max=M_max,
            )
        ).reshape(m1_mesh.shape)

        intensity_marginal = numpy.trapz(intensity, dx=dm, axis=0)

        analytic_marginal = prob.marginal_pdf(
            m_linear, alpha, args.m_min, M_max,
        )
        analytic_marginal /= numpy.trapz(analytic_marginal, dx=dm)

        ax_intensity.loglog(
            m_linear, intensity_marginal,
            color="black", linestyle="-"
        )
        ax_intensity.loglog(
            m_linear, analytic_marginal,
            color="black", linestyle="--"
        )


    pdf_grid = prob.joint_pdf(
        m1_mesh, m2_mesh,
        args.alpha_fixed,
        args.m_min, M_max,
    )
    norm = dm*dm * numpy.sum(pdf_grid)
    integrand = pdf_grid * VT / norm


    ctr = ax_integrand.contourf(
        m1_mesh, m2_mesh,
        numpy.log10(integrand).reshape(m1_mesh.shape),
        100, cmap=matplotlib.cm.viridis,
    )
    fig_integrand.colorbar(ctr)

    ax_integrand.set_ylim([args.m_min, 0.5*M_max])
    ax_integrand.set_xlabel(r"$m_1$")
    ax_integrand.set_ylabel(r"$m_2$")


    ax_mean_vs_alpha.plot(
        alphas, means,
    )

    ax_mean_vs_alpha.set_xlabel(r"$\alpha$")
    ax_mean_vs_alpha.set_ylabel(r"$\mu$ for $\mathcal{R} = 1$")

    fig_likelihood.savefig(args.output_plot_likelihood)
    fig_intensity.savefig(args.output_plot_intensity)
    fig_integrand.savefig(args.output_plot_integrand)
    fig_mean_vs_alpha.savefig(args.output_plot_mean_vs_alpha)


if __name__ == "__main__":
    import sys
    sys.exit(_main(sys.argv[1:]))
