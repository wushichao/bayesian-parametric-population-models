r"""
This module implements a method to sample from the posterior of an inhomogenous
Poisson process. It assumes some user-provided parametric (parameterized by
:math:`\Lambda`) Poisson intensity function :math:`\rho(\lambda\mid\Lambda)`,
whose integral over a region of parameter space :math:`\lambda` and an implicit
configuration space will result in the expected number of events in that region,
:math:`\mu`. Parameter-dependent selection effects are allowed by giving the
user the ability to independently specify :math:`\mu(\Lambda)`, the expected
observed number of events, with selection effects fully considered.

The inference depends on one's observation of :math:`N` events (labeled
:math:`n \in \{1, \ldots, N\}`), with parameters described by
likelihoods :math:`p(d_n \mid \lambda)`. With all this in hand, the likelihood
for a given population parameter :math:`\Lambda` is given by:

.. math::
   \mathcal{L}(\Lambda) \equiv
   p(d_1, \ldots, d_N \mid \Lambda) \propto
   \exp[-\mu(\Lambda)] \,
   \prod_{n=1}^N
   \int \mathrm{d}\lambda \, p(d_n \mid \lambda) \, \rho(\lambda\mid\Lambda)

The posterior distribution is proportional to the product of the likelihood and
a prior :math:`\pi(\Lambda)`:

.. math::
   p(\Lambda \mid d_1, \ldots, d_N) \propto \pi(\Lambda) \mathcal{L}(\Lambda)

To avoid computing the normalization constant, the posterior is described
through a number of samples drawn from it via Goodman & Weare's Affine Invariant
Markov chain Monte Carlo sampler [GoodmanWeare]_, as implemented in the
`emcee <http://dfm.io/emcee/>`_ Python library.

The integral in the likelihood expression is evaluated, up to another
normalization constant, by rewriting the event likelihoods
:math:`p(d_n \mid \lambda)` in terms of event posteriors
:math:`p(\lambda \mid d_n)` taken with respect to some reference prior
:math:`\pi_n(\lambda)`:

.. math::
   \int \mathrm{d}\lambda \,
     p(d_n \mid \lambda) \, \rho(\lambda\mid\Lambda)
   \propto
   \int \mathrm{d}\lambda \,
   \frac{p(\lambda \mid d_n) \, \rho(\lambda\mid\Lambda)}{\pi_n(\lambda)}

which allows us to convert this into a Monte Carlo integral over samples from
the posteriors :math:`\lambda_{n,i} \sim p(\lambda \mid d_n)` (along with their
respective priors :math:`\pi_{n,i} = \pi_n(\lambda_{n,i})`), as one would obtain
in a parameter estimation procedure

.. math::
   \int \mathrm{d}\lambda \,
   \frac{p(\lambda \mid d_n) \, \rho(\lambda\mid\Lambda)}{\pi_n(\lambda)}
   \approx
   \frac{1}{N_{\mathrm{samples}}}
   \sum_{i=1}^{N_{\mathrm{samples}}}
     \frac{\rho(\lambda_{n,i}\mid\Lambda)}{\pi_{n,i}}

All of these pieces are put together in this module's function :func:`run_mcmc`,
which takes as input the intensity function :math:`\rho(\lambda\mid\Lambda)`;
the the expected value function :math:`\mu(\Lambda)`; a list of lists of
samples from the :math:`N` detections' posteriors
:math:`(\{\lambda_{1,i}\}, \ldots, \{\lambda_{N,i}\})` and their respective
priors :math:`(\{\pi_{1,i}\}, \ldots, \{\pi_{N,i}\})`; a function which
evaluates to the log of the population prior up to a normalization constant
:math:`\log\pi(\Lambda)`; and the initial state of the MCMC chain
:math:`\Lambda_0` for each of the ensemble walkers which will evolve in
parallel. One also has the option of fixing some of the components of
:math:`\Lambda` to constants, or as duplicates of each other.

The resulting posterior samples may have a burnin period, and will likely have
some autocorrelation, so one is encouraged to extract usable samples either
through one's own methods, or by the methods provided in
:mod:`pop_models.sample_extraction`, and in the command line script
``pop_models_extract_samples``.

.. [GoodmanWeare]
   Ensemble samplers with affine invariance,
   Jonathan Goodman and Jonathan Weare,
   Commun. Appl. Math. Comput. Sci., Volume 5, Number 1 (2010), 65-80
   DOI:
   `10.2140/camcos.2010.5.65 <https://doi.org/10.2140/camcos.2010.5.65>`_
"""
from __future__ import division, print_function

import numpy



def run_mcmc(
        intensity_fn, expval_fn, event_posterior_samples,
        log_prior_fn,
        init_state,
        param_names,
        constants=None, duplicates=None,
        event_posterior_sample_priors=None,
        args=None, kwargs=None,
        before_prior_aux_fn=None, after_prior_aux_fn=None,
        out_pos=None, out_log_prob=None,
        nsamples=100,
        rand_state=None,
        nthreads=1, pool=None, runtime_sortingfn=None,
        verbose=False,
        dtype=numpy.float64,
    ):
    r"""
    Samples from the posterior distribution for an inhomogeneous Poisson process
    using Goodman & Weare's Affine Invariant Markov chain Monte Carlo sampler.
    Posterior takes the form

    .. math::
       p(\Lambda \mid d_1, \ldots, d_N) \propto
       \pi(\Lambda) \,
       \exp[-\mu(\Lambda)] \,
       \prod_{n=1}^N
         \int \mathrm{d}\lambda \,
           p(d_n \mid \lambda) \, \rho(\lambda\mid\Lambda)

    with user-provided functions for :math:`\rho(\lambda\mid\Lambda)`,
    :math:`\mu(\Lambda)`, and :math:`\pi(\Lambda)`, as well as samples from the
    posterior :math:`p(\lambda \mid d_n)` and their respective priors
    :math:`\pi_n(\lambda)`, which are used to evaluate the integral (up to a
    proportionality constant) using Monte Carlo.

    :math:`\rho(\lambda\mid\Lambda)` is specified by the argument
    ``intensity_fn``, :math:`\mu(\Lambda)` is specified by ``expval_fn``,
    :math:`\pi(\Lambda)` is specified by ``log_prior_fn`` (which instead gives
    its logarithm, up to an additive constant). A list of posterior samples for
    each of the observed events indexed by :math:`n \in \{1, 2, \ldots, N\}` is
    specified by ``event_posterior_samples``, where each element should be an
    array of shape :math:`N_{\mathrm{samples},n} \times \mathrm{dim}(\lambda)`
    (Note: each one is allowed to have a different number of samples, but not a
    different form for :math:`\lambda`). Optionally one can provide a list of
    priors for each event, which should be a list of the same length, containing
    arrays with the same :math:`N_{\mathrm{samples},n}` for each element. If one
    does not provide this list, no division by the prior is performed, which is
    equivalent to using an (improper) prior which is uniform everywhere.
    Similarly, one can provide a list of priors, but for one or more of the
    elements use ``None`` instead of the array of priors, and just those events
    will use the (improper) uniform prior.

    Each of the user-provided functions takes one or both of :math:`\lambda` and
    :math:`\Lambda` as its first arguments, in order, as one would expect from
    their mathematical forms. :math:`\lambda` is always provided as an array of
    shape :math:`N_{\mathrm{samples}} \times \mathrm{dim}(\lambda)`, and should
    allow for arbitrary values of :math:`N_{\mathrm{samples}}`. :math:`\Lambda`
    is always provided as a list or tuple of length
    :math:`\mathrm{dim}(\Lambda)`. After these arguments, the functions always
    take an argument ``aux_info``, which is ``None`` by default, but may be
    replaced before the prior is evaluated by the return value of the optional
    user-provided function ``before_prior_aux_fn``, and may be replaced after
    the prior is evaluated (but before the intensity and expected value
    functions are) by another optional user-provided function
    ``after_prior_aux_fn``. This is meant to store quantities which are
    functions of :math:`\Lambda`, and are needed by the other user-provided
    functions, which you do not want re-computed by each call of those
    functions, for the sake of saving computing time. For instance, you may be
    sampling :math:`\Lambda` in one coordinate system, but your functions
    actually make use of it in another coordinate system, and you do not want
    to re-compute the transformation multiple times. Another use case is
    computing things like normalization constants, which vary with
    :math:`\Lambda`.

    For example usages, see the production-ready powerlaw implementation in
    :mod:`powerlaw.mcmc`, and the simple test case in
    :doc:`/examples/simple_mcmcs/plot_gaussian_population`.

    Execution sequence for log(posterior) function evaluated at each step of the
    MCMC is described by the following pseudocode (OUT OF DATE):

    .. image:: /_static/api/pop_models/mcmc/run_mcmc/log_posterior_pseudocode.png


    :param function intensity_fn:

    :param function expval_fn:

    :param array_like event_posterior_samples:

    :param array_like init_state:

    :param list param_names:

    :param dict constants: (optional)

    :param dict duplicates: (optional)

    :param array_like event_posterior_sample_priors: (optional)

    :param list args: (optional)

    :param dict kwargs: (optional)

    :param function aux_fn: (optional)

    :param array_like out_pos: (optional)

    :param array_like out_log_prob: (optional)

    :param int nsamples: (default 100)

    :param numpy.random.RandomState rand_state: (optional)

    :param int nthreads: (optional)

    :param multiprocessing.Pool pool: (optional)

    :param function runtime_sortingfn: (optional)

    :param bool verbose: (optional)

    :param type dtype: (optional)

    :return: array_like, shape (n_samples, n_walkers, n_params)
        MCMC chain for each ensemble walker. Element ``[i,j,k]`` is the value of
        the ith sample in the chain, for the jth walker, for the kth free
        parameter.

    :return: array_like, shape (n_samples, n_walkers)
        Values of the (non-normalized) log-posterior function corresponding to
        each step in the MCMC chain for each ensemble walker. Element ``[i,j]``
        is the log-posterior for the ith sample in the chain, for the jth
        walker.
    """
    import sys
    import numpy
    import emcee

    from . import posterior
    from . import utils

    # If no pool is provided, use utils.VectorizedPool
    if pool is None:
        pool = utils.VectorizedPool()

    # If no args or kwargs provided, set as empty list/dict
    if args is None:
        args = []
    if kwargs is None:
        kwargs = {}

    # If no constants provided, set as empty dict
    if constants is None:
        constants = {}

    # If no duplicates provided, set as empty dict
    if duplicates is None:
        duplicates = {}

    # Ensure no duplicates refer to other duplicates
    assert set(duplicates.keys()).isdisjoint(set(duplicates.values()))

    # Count the number of free parameter dimensions
    ndim = len(param_names) - len(constants) - len(duplicates)

    # Count the number of walkers
    nwalkers = len(init_state)
    # Count the number of individual events
    nindiv = len(event_posterior_samples)

    # We're going to iterate over the samples and weights together, so if there
    # are no weights, we at least need to make it into a list of the proper
    # size.
    if event_posterior_sample_priors is None:
        event_posterior_sample_priors = [None for _ in event_posterior_samples]

    # Ensure samples all have same dimensionality
    ndim_indiv = None
    for samples in event_posterior_samples:
        S, D = numpy.shape(samples)

        if ndim_indiv is None:
            ndim_indiv = D

        assert ndim_indiv == D

    # Ensure number of dimensions equals the number of dimensions in the initial
    # state.
    assert ndim == len(init_state[0])

    # Initialize output arrays if not provided.
    # Otherwise check provided arrays have proper shape.
    if out_pos is None:
        out_pos = numpy.empty((nsamples, nwalkers, ndim), dtype=dtype)
    else:
        assert numpy.shape(out_pos) == (nsamples, nwalkers, ndim)
    if out_log_prob is None:
        out_log_prob = numpy.empty((nsamples, nwalkers), dtype=dtype)
    else:
        assert numpy.shape(out_log_prob) == (nsamples, nwalkers)


    sampler_args = (
        param_names, constants, duplicates,
        intensity_fn, expval_fn, log_prior_fn,
        event_posterior_samples, event_posterior_sample_priors,
        before_prior_aux_fn, after_prior_aux_fn,
        args, kwargs,
    )

    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, posterior.log_posterior,
        args=sampler_args,
        threads=nthreads, pool=pool, runtime_sortingfn=runtime_sortingfn,
    )
    sample_iter = sampler.sample(
        init_state,
        iterations=nsamples, rstate0=rand_state,
        storechain=False,
    )

    if verbose:
        progress_pct = 0
        def display_progress(p, s):
            print(
                "Progress: {p}%; Samples: {s}".format(p=p, s=s),
                file=sys.stderr,
            )
        display_progress(progress_pct, 0)


    for i, result in enumerate(sample_iter):
        pos = result[0]
        log_post = result[1]

        out_pos[i] = pos
        out_log_prob[i] = log_post

        if verbose:
            new_progress_pct = i / nsamples * 100
            if new_progress_pct >= progress_pct + 1:
                progress_pct = int(new_progress_pct)
                display_progress(progress_pct, i)


    return out_pos, out_log_prob
