"""

"""

from __future__ import division



def sample_ppd(
        sampler, n_samples,
        post_samples, post_weights=None,
        random_state=None,
    ):
    """
    Samples from a posterior predictive distribution.

    sampler = rvs(N, params, rand_state=None)
    """
    import numpy
    import scipy.stats

    # Check number of dimensions from sampler.
    test_sample = sampler(1, post_samples[0], random_state=random_state)
    _, n_dim = numpy.shape(test_sample)

    # Check number of parameters, and number of posterior samples
    n_post, n_params = numpy.shape(post_samples)


    # Determine probability of drawing using each posterior sample.
    # If ``post_weights`` is None, this is just 1/n_post, otherwise it will be
    # proportional to ``post_weights``, but properly normalized to 1.0.
    if post_weights is None:
        weights = numpy.empty(n_post, dtype=post_samples.dtype)
        weights[:] = 1.0 / n_post
    else:
        weights = post_weights / numpy.sum(post_weights)

    # Determine number of samples to draw from ``sampler`` for each sample in
    # ``post_samples``.
    counts = scipy.stats.multinomial.rvs(
        n_samples, weights,
        random_state=random_state,
    )

    # Draw samples from the PPD.
    ppd_samples = numpy.empty((n_samples, n_dim), dtype=test_sample.dtype)
    n_drawn = 0
    for count, post_sample in zip(counts, post_samples):
        n_drawn_next = n_drawn + count
        ppd_samples[n_drawn:n_drawn_next] = sampler(
            count, post_sample,
            random_state=random_state,
        )
        n_drawn = n_drawn_next

    return ppd_samples
