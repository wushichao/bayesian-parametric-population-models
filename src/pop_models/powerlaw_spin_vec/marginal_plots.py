from __future__ import division, print_function

def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "output_fmt",
        help="Format string which evaluates to filename to store plot in. "
             "Should have the string '{param}' in it, which will be replaced "
             "by each free parameter of the MCMC.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in each plot.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--fig-size",
        type=float, default=(8,8), nargs=2,
        help="Dimensions for figures.",
    )
    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import json
    import h5py
    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt
    import seaborn as sns

    matplotlib.rcParams["text.usetex"] = True
    matplotlib.rcParams["text.latex.preamble"] = [
        "\\usepackage{amsmath}",
        "\\usepackage{amssymb}",
    ]

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    from . import prob
    from . import plot_config
    from . import utils

    with h5py.File(args.posteriors, "r") as post_file:
        posteriors = post_file["pos"]

        param_names = prob.param_names

        pop_prior_settings = json.loads(post_file.attrs["priors"])
        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        params = utils.get_params(
            posteriors, constants, duplicates, param_names,
        )
        params = dict(zip(param_names, params))


        # Determine sample weights
        if (not args.rate_logU_to_jeffereys) or ("log10_rate" in constants):
            weights = None
        else:
            weights = numpy.sqrt(numpy.power(10.0, params["log10_rate"]))

        # Make plots
        for param_name in param_names:
            # Skip over constants and duplicates
            if (param_name in constants) or (param_name in duplicates):
                continue

            fig, ax = plt.subplots(figsize=args.fig_size)

            name = plot_pdf(
                ax,
                params[param_name], weights,
                param_name,
                args.n_samples,
                pop_prior_settings,
            )

            fig.tight_layout()
            fig.savefig(args.output_fmt.format(param=name))


def plot_pdf(ax, param, weights, param_name, n_samples, pop_prior_settings):
    return {
        "log10_rate": _plot_rate,
        "alpha_m": _plot_alpha_m,
        "m_min": _plot_m_min,
        "m_max": _plot_m_max,
        "E_chi1": _plot_E_chi1,
        "Var_chi1": _plot_Var_chi1,
        "E_chi2": _plot_E_chi2,
        "Var_chi2": _plot_Var_chi2,
        "mu_cos1": _plot_mu_cos1,
        "sigma_cos1": _plot_sigma_cos1,
        "mu_cos2": _plot_mu_cos2,
        "sigma_cos2": _plot_sigma_cos2,
    }[param_name](ax, param, weights, n_samples, pop_prior_settings)


def _plot_rate(ax, log10_rate, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    rate_label = plot_config.labels["rate"]
    rate_units = plot_config.units["rate"]

    # Determine plot limits from prior if possible, else use sample range.
    pop_prior_settings = pop_prior_settings["log10_rate"]
    dist = pop_prior_settings["dist"]
    if dist == "uniform":
        log_rate_min = pop_prior_settings["params"]["min"]
        log_rate_max = pop_prior_settings["params"]["max"]
    else:
        log_rate_min = numpy.floor(numpy.min(log10_rate))
        log_rate_max = numpy.ceil(numpy.max(log10_rate))

    log10_rate_smooth = numpy.linspace(log_rate_min, log_rate_max, n_samples)
    rate_smooth = numpy.power(10.0, log10_rate_smooth)

    dP_dlog10_rate = gaussian_kde(
        log10_rate, weights=weights,
        bw_method="scott",
    )
    dP_dlog10_rate_smooth = dP_dlog10_rate(log10_rate_smooth)

    dP_drate_smooth = dP_dlog10_rate_smooth / (rate_smooth * numpy.log(10.0))


    ax.semilogx(rate_smooth, rate_smooth*dP_drate_smooth)

    ax.set_xlabel(
        r"${rate_label} [{rate_units}]$"
        .format(rate_label=rate_label, rate_units=rate_units)
    )
    ax.set_ylabel(
        r"${rate_label} p({rate_label})$"
        .format(rate_label=rate_label)
    )

    return "rate"


def _plot_alpha_m(ax, alpha, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    alpha_label = plot_config.labels["alpha_m"]

    # Determine plot limits from prior.
    alpha_min = pop_prior_settings["alpha_m"]["params"]["min"]
    alpha_max = pop_prior_settings["alpha_m"]["params"]["max"]

    alpha_smooth = numpy.linspace(alpha_min, alpha_max, n_samples)

    dP_dalpha = gaussian_kde(
        alpha, weights=weights,
        bw_method="scott",
    )
    dP_dalpha_smooth = dP_dalpha(alpha_smooth)

    ax.plot(alpha_smooth, dP_dalpha_smooth)

    ax.set_xlabel(
        r"${alpha_label}$"
        .format(alpha_label=alpha_label)
    )
    ax.set_ylabel(
        r"$p({alpha_label})$"
        .format(alpha_label=alpha_label)
    )

    return "alpha_m"


def _plot_m_min(ax, m_min, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    m_min_label = plot_config.labels["m_min"]

    # Determine plot limits from prior.
    m_min_min = pop_prior_settings["m_min"]["params"]["min"]
    m_min_max = pop_prior_settings["m_min"]["params"]["max"]

    m_min_smooth = numpy.linspace(m_min_min, m_min_max, n_samples)

    dP_dm_min = gaussian_kde(
        m_min, weights=weights,
        bw_method="scott",
    )
    dP_dm_min_smooth = dP_dm_min(m_min_smooth)

    ax.plot(m_min_smooth, dP_dm_min_smooth)

    ax.set_xlabel(
        r"${m_min_label}$"
        .format(m_min_label=m_min_label)
    )
    ax.set_ylabel(
        r"$p({m_min_label})$"
        .format(m_min_label=m_min_label)
    )

    return "m_min"


def _plot_m_max(ax, m_max, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    m_max_label = plot_config.labels["m_max"]


    # Determine plot limits from prior.
    m_max_min = pop_prior_settings["m_max"]["params"]["min"]
    m_max_max = pop_prior_settings["m_max"]["params"]["max"]

    m_max_smooth = numpy.linspace(m_max_min, m_max_max, n_samples)

    dP_dm_max = gaussian_kde(
        m_max, weights=weights,
        bw_method="scott",
    )
    dP_dm_max_smooth = dP_dm_max(m_max_smooth)

    ax.plot(m_max_smooth, dP_dm_max_smooth)

    ax.set_xlabel(
        r"${m_max_label}$"
        .format(m_max_label=m_max_label)
    )
    ax.set_ylabel(
        r"$p({m_max_label})$"
        .format(m_max_label=m_max_label)
    )

    return "m_max"


def _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        param_name,
    ):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    param_label = plot_config.labels[param_name]

    # Determine plot limits from prior.
    param_min = pop_prior_settings[param_name]["params"]["min"]
    param_max = pop_prior_settings[param_name]["params"]["max"]

    param_smooth = numpy.linspace(param_min, param_max, n_samples)

    dP_dparam = gaussian_kde(
        param, weights=weights,
        bw_method="scott",
    )
    dP_dparam_smooth = dP_dparam(param_smooth)

    ax.plot(param_smooth, dP_dparam_smooth)

    ax.set_xlabel(
        r"${param_label}$"
        .format(param_label=param_label)
    )
    ax.set_ylabel(
        r"$p({param_label})$"
        .format(param_label=param_label)
    )

    return param_name


def _plot_E_chi1(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "E_chi1",
    )

def _plot_Var_chi1(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "Var_chi1",
    )

def _plot_E_chi2(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "E_chi2",
    )

def _plot_Var_chi2(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "Var_chi2",
    )

def _plot_mu_cos1(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "mu_cos1",
    )

def _plot_sigma_cos1(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "sigma_cos1",
    )

def _plot_mu_cos2(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "mu_cos1",
    )

def _plot_sigma_cos2(ax, param, weights, n_samples, pop_prior_settings):
    return _plot_spin_vec_param(
        ax, param, weights, n_samples, pop_prior_settings,
        "sigma_cos1",
    )


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(main(raw_args))
