r"""
This module contains functions for computing auto-correlation of MCMC chains,
in order to ensure samples are independent.
"""

from __future__ import division

class ConvergenceWarning(RuntimeWarning):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def acorr(X, lag, axis=0, reterr=False):
    r"""
    Computes the auto-correlation of the sequence ``X`` given a lag ``lag``.

    .. math::
       \rho_K =
       \frac{N}{N-K} \,
       \frac{
         \sum_{n=1}^{N-K}
           (X_n - \langle X \rangle)
           (X_{n+K} - \langle X \rangle)
       }{
         \sum_{n=1}^N
           (X_n - \langle X \rangle)^2
       }

    If ``reterr`` is ``True``, the variance will also be estimated.  The
    assumption is that the denominator has practically no error (for a large
    enough sample this is a good assumption), while the numerator holds all the
    error.  Also note that the numerator is just a sample variance, so we use
    the standard error for a sample variance (assuming Gaussianity) [VarEst]_

    .. math::
       \mathrm{Var}[s^2] = \frac{2 \sigma^4}{n - 1} \approx \frac{2 s^4}{n - 1}

    .. [VarEst]
       Variance: Distribution of the sample variance
       `Wikipedia <https://en.wikipedia.org/wiki/Variance#Distribution_of_the_sample_variance>`_

    """
    import numpy

    N = numpy.shape(X)[axis]
    dX = X - numpy.mean(X, axis=axis)

    result = numpy.mean(dX[lag:] * dX[:N-lag], axis=axis)

    if reterr:
        err = 2 * numpy.power(result, 2.0) / (N-lag-1)

    result /= numpy.mean(numpy.square(dX), axis=axis)

    if reterr:
        return result, err
    else:
        return result



def acorr_time(chains, m=5, retall=False):
    r"""
    Returns the integrated autocorrelation time :math:`\tau_K`,

    .. math::
       \tau_K = 1 + 2 \sum_{i=1}^K \rho_i

    where :math:`\rho_i` is the autocorrelation with lag :math:`i` (see
    :func:`acorr`),

    .. math::
       K = \mathrm{argmin}_{K'}\left\{
         m \tau_{K'} \leq K'
       \right\},

    and :math:`m` is a user-provided integer, which defaults to 5.

    Given an ensemble of chains ``chains``, this uses the mean of all the chains
    rather than any individual chain.

    Returns the integrated auto-correlation time for the worst dimension if
    `retall` is False, and returns that along with the individual
    autocorrelations as well as the candidate integrated autocorrelationt times
    (for lower `K`'s).
    """
    import warnings
    import numpy

    n_samples, n_walkers, n_dim = numpy.shape(chains)

    # Take the mean over all walkers
    mean_chain = numpy.mean(chains, axis=1)


    # Initialize array that will hold the auto-correlation for each lag.
    # We will not necessarily evaluate it at every lag, but we need to
    # initialize the array, so we fill it with nan's.
    acorrs = numpy.empty((n_samples, n_dim), dtype=numpy.float64)
    acorrs[0] = numpy.nan

    # Initialize cumulative sum of individual autocorrelations.
    # Element `i` should contain the sum of acorrs[1] through acorrs[i]
    acorrs_cumsum = numpy.empty_like(acorrs)
    acorrs_cumsum[0] = 0.0

    # Initialize integrated autocorrelation times.
    acorr_times = numpy.empty_like(acorrs)
    acorr_times[0] = numpy.nan

    # Compute standard errors as well
    if retall:
        acorr_errs = numpy.empty_like(acorrs)
        acorr_errs[0] = numpy.nan

        acorr_errs_cumsum = numpy.empty_like(acorrs_cumsum)
        acorr_errs_cumsum[0] = 0.0

        acorr_time_errs = numpy.empty_like(acorr_times)
        acorr_time_errs[0] = numpy.nan

    # Compute integrated autocorrelation times until convergence criterion met.
    for K in range(1, n_samples):
        result = acorr(mean_chain, K, reterr=retall)
        if retall:
            acorrs[K], acorr_errs[K] = result
            acorr_errs_cumsum[K] = acorr_errs_cumsum[K-1] + acorr_errs[K]
            acorr_time_errs[K] = 2.0*acorr_errs_cumsum[K]
        else:
            acorrs[K] = acorr(mean_chain, K)

        acorrs_cumsum[K] = acorrs_cumsum[K-1] + acorrs[K]
        acorr_times[K] = 1.0 + 2.0*acorrs_cumsum[K]

        if numpy.all(K >= m*acorr_times[K]):
            K_final = K
            break
    else:
        K_final = n_samples-1
        warnings.warn(ConvergenceWarning(
            "Auto-correlation calculation not yet converged."
        ))

    acorr_worst = int(numpy.ceil(numpy.max(acorr_times[K_final])))

    if retall:
        return (
            acorr_worst,
            (acorrs[:K_final], acorr_errs[:K_final]),
            (acorr_times[:K_final], acorr_time_errs[:K_final]),
        )
    else:
        return acorr_worst
