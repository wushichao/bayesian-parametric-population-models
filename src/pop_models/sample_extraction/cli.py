from __future__ import division, print_function

__description = r"""\
Extract independent samples from MCMC chains by removing burnin samples and
thinning based on auto-correlations.  Also provides options for diagnostic
plots.
"""


color_cycle = [
    "#1f77b4",
    "#ff7f0e",
    "#2ca02c",
    "#d62728",
    "#9467bd",
    "#8c564b",
    "#e377c2",
    "#7f7f7f",
    "#bcbd22",
    "#17becf",
]
linestyle_cycle = [
    "solid",
    "dashed",
    "dashdot",
    "dotted",
]


def make_parser():
    import argparse

    parser = argparse.ArgumentParser(description=__description)

    # TODO: Allow multiple input chains
    parser.add_argument(
        "input_chains",
        help="HDF5 file containing raw MCMC samples from multiple walkers.",
    )
    parser.add_argument(
        "output_samples",
        help="Creates a new HDF5 file storing only the independent samples.  "
             "Walker dimension is compressed.",
    )

    parser.add_argument(
        "--max-posterior-burnin",
        nargs="*",
        help="Remove burnin according to the max-posterior method.",
    )
    parser.add_argument(
        "--gelman-rubin",
        nargs="*",
        help="Remove burnin according to the Gelman-Rubin R-statistic.  "
             "Note that this is not a valid statistic for ensemble samplers, "
             "including the one used by default in pop_models, so its use is "
             "not recommended.",
    )
    parser.add_argument(
        "--auto-correlation",
        nargs="*",
        help="Remove burnin according to an auto-correlation cut applied on "
             "each walker individually.  Note that this is not a meaningful "
             "statistic in general, and its use is not recommended in any case."
    )
    parser.add_argument(
        "--integrated-auto-correlation",
        nargs="*",
        help="Remove burnin according to the integrated auto-correlation time "
             "of the entire ensemble.  This is valid only for ensemble "
             "samplers, including the default in pop_models, so its use is "
             "recommended.",
    )

    parser.add_argument(
        "--full-chain-plot",
        help="Provide the filename to store a plot summarizing all of the "
             "chains.  Burnin samples are shown in red, with a grey "
             "background, post-burnin samples are shown in black, and "
             "specifically those that survive auto-correlation thinning are "
             "shown as blue dots."
    )
    parser.add_argument(
        "--cleaned-chain-plot",
        help="Provide the filename to store a plot showing only the final "
             "extracted samples from all walkers.",
    )
    parser.add_argument(
        "--max-posterior-burnin-plot",
        help="Provide the filename to store a plot displaying the distribution "
             "of burnin lengths computed for each walker, according to the "
             "max-posterior method.  Only the worst walker's burnin length is "
             "used, but this allows one to assess the overall performance of "
             "the ensemble.",
    )
    parser.add_argument(
        "--gelman-rubin-plot",
        help="Provide the filename to store a plot displaying the Gelman-Rubin "
             "R-statistic computed for each parameter, as a function of the "
             "chain length, taking only the first 'n' samples.",
    )
    parser.add_argument(
        "--auto-correlation-plot",
        help="Provide the filename to store a plot displaying the "
             "auto-correlation computed for each parameter, as a function of "
             "the lag time.  These are computed separately for each walker, so "
             "the median and smallest/largest values are shown as a solid line "
             "and transparent bands.",
    )
    parser.add_argument(
        "--integrated-auto-correlation-plot",
        help="Provide the filename to store a plot displaying the integrated "
             "auto-correlation computed for each parameter, as a function of "
             "the maximum lag time 'K' used in its calculation.",
    )

    parser.add_argument(
        "--keep-all",
        action="store_true",
        help="Regardless of analyses performed, extract samples as if there "
             "were no burnin, and all samples were un-correlated.  This is "
             "intended to be used for testing purposes only, and is typically "
             "used when only a very short chain is generated, and one still "
             "wants to perform post-processing as if it were longer, as a "
             "test.",
    )
    parser.add_argument(
        "--fixed-burnin",
        type=int,
        help="Regardless of analyses performed, extract samples as if the "
             "burnin were exactly the number specified.  Can be used as a more "
             "conservative alternative to --keep-all, or can even be used to "
             "discard additional samples if the automated procedures are "
             "insufficient."
    )
    parser.add_argument(
        "--fixed-auto-correlation",
        type=int,
        help="Regardless of analyses performed, extract samples as if the "
             "auto-correlation time were exactly the number specified.  Can be "
             "used as a more conservative alternative to --keep-all, or can "
             "even be used to discard additional samples if the automated "
             "procedures are insufficient."
    )

    parser.add_argument(
        "--skip-first-n-in-plotting",
        default=0, type=int,
        help="Skip this many samples at the beginning for plotting purposes "
             "only.  This is useful for chain plots where the first few "
             "iterations have such low log probability that they make the rest "
             "of the plot too small to see.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    parser.add_argument(
        "-f", "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    return parser


def parse_options(options):
    from ast import literal_eval

    if options is None:
        return {}

    out = {}

    for option in options:
        key, val_str = option.split("=")
        try:
            val = literal_eval(val_str)
        except ValueError:
            val = val_str

        out[key] = val

    return out


def _main(raw_args=None):
    import six
    import sys

    import h5py
    import numpy

    from . import acorr, gelman_rubin, integrated_acorr, max_posterior

    if raw_args is None:
        raw_args = sys.argv[1:]

    cli_args = make_parser().parse_args(raw_args)

    # Import matplotlib if any plots are to be created.
    plot_args = [
        cli_args.full_chain_plot,
        cli_args.cleaned_chain_plot,
        cli_args.max_posterior_burnin_plot,
        cli_args.gelman_rubin_plot,
        cli_args.auto_correlation_plot,
        cli_args.integrated_auto_correlation_plot,
    ]
    if any(p is not None for p in plot_args):
        import matplotlib
        matplotlib.use(cli_args.mpl_backend)
        import matplotlib.pyplot


    # Input validation
    if cli_args.keep_all:
        extras = []

        if cli_args.fixed_burnin is not None:
            extras.append("--fixed-burnin")
        if cli_args.fixed_auto_correlation is not None:
            extras.append("--fixed-auto-correlation")

        if len(extras) > 0:
            raise Exception(
                "Cannot use --keep-all in conjunction with " +
                " or ".join(extras)
            )

        del extras

    output_mode = "w" if cli_args.force else "w-"

    max_posterior_burnin_options = parse_options(cli_args.max_posterior_burnin)
    gelman_rubin_options = parse_options(cli_args.gelman_rubin)
    auto_correlation_options = parse_options(cli_args.auto_correlation)
    integrated_auto_correlation_options = parse_options(
        cli_args.integrated_auto_correlation
    )

    with h5py.File(cli_args.input_chains, "r") as input_chains:
        with h5py.File(cli_args.output_samples, output_mode) as output_samples:
            # Load arrays which describe the actual samplers.
            log_prob = input_chains["log_prob"].value
            pos = input_chains["pos"].value

            # Determine dimension, size, and number of chains.
            n_samples, n_walkers, n_dim = numpy.shape(pos)
            n_samples_original = n_samples*n_walkers

            # Copy attributes
            for k, v in six.iteritems(input_chains.attrs):
                output_samples.attrs[k] = v

            # Copy any datasets and groups that aren't "log_prob" or "pos", as
            # they will be modified and then copied.
            for name, value in six.iteritems(input_chains):
                # Skip datasets we're already going to make later.
                if name not in ["pos", "log_prob"]:
                    input_chains.copy(name, output_samples)

            # Burnin analysis
            if cli_args.max_posterior_burnin is not None:
                n_burnin, n_burnins = max_posterior.compute_burnin(
                    log_prob, n_dim,
                    retall=True,
                )
                sqrt_R_stats = None
                if cli_args.skip_first_n_in_plotting > n_burnin:
                    raise ValueError(
                        "--skip-first-n-in-plotting value is too high, "
                        "exceeds the burnin: {}"
                        .format(n_burnin)
                    )
            elif cli_args.gelman_rubin is not None:
                n_gelman_rubin, sqrt_R_stats = gelman_rubin.burnin_time(
                    pos, return_R_stat=True, **gelman_rubin_options
                )
                n_burnins = None
                print("{n} burnin samples removed".format(n=n_gelman_rubin))
                print(
                    "sqrt(R-statistics) are:",
                    ", ".join([str(x) for x in sqrt_R_stats[n_gelman_rubin]]),
                )
            else:
                n_gelman_rubin, sqrt_R_stats = 0, None

            if cli_args.keep_all:
                n_burnin = 0
            elif cli_args.fixed_burnin is not None:
                n_burnin = cli_args.fixed_burnin

            log_prob_noburnin = log_prob[n_burnin:]
            pos_noburnin = pos[n_burnin:]

            # Auto-correlation analysis
            if cli_args.integrated_auto_correlation is not None:
                (
                    n_acorr,
                    (acorrs, acorr_errs),
                    (acorr_times, acorr_time_errs),
                ) = (
                    integrated_acorr.acorr_time(
                        pos_noburnin, retall=True,
                        **integrated_auto_correlation_options
                    )
                )
                delta_n_acorr = numpy.tile(n_acorr, (n_walkers, n_dim))
                print(
                    "Auto-correlation time is {dn} for the worst parameter."
                    .format(dn=n_acorr)
                )
                print(
                    "Auto-correlation time for all parameters are",
                    ", ".join([str(x) for x in acorr_times[n_acorr]]),
                )
            elif cli_args.auto_correlation is not None:
                delta_n_acorr, acorrs = acorr.acorr_time(
                    pos_noburnin, return_acorr=True, **auto_correlation_options
                )
                acorr_errs = None
            else:
                delta_n_acorr = numpy.ones((n_walkers, n_dim), dtype=int)

            if cli_args.keep_all:
                n_thins = numpy.ones((n_walkers, n_dim), dtype=int)
            elif cli_args.fixed_auto_correlation is not None:
                n_thins = numpy.tile(
                    cli_args.fixed_auto_correlation, (n_walkers, n_dim),
                )
            else:
                n_thins = delta_n_acorr

            # Extract non-auto-correlated samples.
            n_samples_extracted = 0
            iterations_extracted = []
            log_prob_extracted = []
            pos_extracted = []
            for w in range(n_walkers):
                # Use worst dimension
                n_thin_candidates = n_thins[w]

                # This walker has no usable samples
                if numpy.count_nonzero(n_thin_candidates) != n_dim:
                    continue

                n_thin = max(n_thin_candidates)

                l = log_prob_noburnin[::n_thin, w]
                p = pos_noburnin[::n_thin, w]
                i = numpy.arange(n_burnin, n_samples, n_thin)

                n_samples_extracted += len(l)

                log_prob_extracted.append(l)
                pos_extracted.append(p)
                iterations_extracted.append(i)

                del n_thin, l, p, i

            n_walkers_kept = len(log_prob_extracted)

            no_acorr = (
                (cli_args.auto_correlation is None) and
                (cli_args.integrated_auto_correlation is None)
            )
            if no_acorr:
                iterations_extracted = None

            print("Original samples:", n_samples_original)
            print("Extracted samples:", n_samples_extracted)
            print(
                "Sampling efficiency: {:.2%}"
                .format(n_samples_extracted/n_samples_original)
            )

            # Plot full chains
            if cli_args.full_chain_plot is not None:
                plot_full_chain(
                    cli_args.full_chain_plot,
                    log_prob, log_prob_extracted, pos, pos_extracted,
                    iterations_extracted,
                    n_burnin,
                    cli_args.skip_first_n_in_plotting,
                )
            # Plot cleaned chains
            if cli_args.cleaned_chain_plot is not None:
                plot_cleaned_chain(
                    cli_args.cleaned_chain_plot,
                    log_prob_extracted, pos_extracted,
                )
            # Plot distribution of burnin times
            if cli_args.max_posterior_burnin_plot is not None:
                fig, ax = max_posterior.plot_burnins(n_burnins)
                fig.tight_layout()
                fig.savefig(cli_args.max_posterior_burnin_plot)
            # Plot (sqrt of) Gelman-Rubin R-statistic
            if cli_args.gelman_rubin_plot is not None:
                plot_gelman_rubin(
                    cli_args.gelman_rubin_plot,
                    sqrt_R_stats, n_gelman_rubin,
                )
            # Plot auto-correlation
            if cli_args.auto_correlation_plot is not None:
                plot_auto_correlation(
                    cli_args.auto_correlation_plot,
                    acorrs, acorr_errs,
                )
            # Plot integrated auto-correlation
            if cli_args.integrated_auto_correlation_plot is not None:
                plot_integrated_auto_correlation(
                    cli_args.integrated_auto_correlation_plot,
                    acorr_times, acorr_time_errs,
                )


            log_prob_out = output_samples.create_dataset(
                "log_prob",
                shape=(n_samples_extracted,),
                dtype=numpy.float64,
            )
            pos_out = output_samples.create_dataset(
                "pos",
                shape=(n_samples_extracted, n_dim),
                dtype=numpy.float64,
            )

            i = 0
            for l, p in zip(log_prob_extracted, pos_extracted):
                n = len(l)

                log_prob_out[i:i+n] = l
                pos_out[i:i+n] = p

                i += n



def plot_full_chain(
        filename,
        log_prob, log_prob_extracted, pos, pos_extracted,
        iterations_extracted,
        n_burnin,
        n_skip,
    ):
    import numpy
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    acorr_analyzed = iterations_extracted is not None

    color_shade = "#7f7f7f"
    color_burnin = "#d62728"
    color_postburnin = "black" if acorr_analyzed else "#1f77b4"
    color_kept = "#1f77b4" if acorr_analyzed else "none"

    # Remove the iterations that we want to skip over
    log_prob = log_prob[n_skip:]
    pos = pos[n_skip:]

    # Now we don't actually want to remove `n_burnin` samples, but only
    # `n_burnin - n_skip` samples, as `n_skip` have already been removed.
    n_remove = n_burnin - n_skip

    n_samples, n_walkers, n_dim = numpy.shape(pos)
    n_subplots = n_dim+1
    iterations = numpy.arange(n_samples)

    datasets = [log_prob] + [pos[...,d] for d in range(n_dim)]
    def datasets_extracted(i):
        if i == 0:
            return log_prob_extracted
        else:
            return [p[:,i-1] for p in pos_extracted]

    alpha_lines = max(0.05, 1.0 / n_dim)
    alpha_points = max(0.10, 1.0 / n_dim)

    fig, axes = plt.subplots(
        n_subplots,
        figsize=(8, 4*n_subplots),
        sharex=True,
    )

    for i, ax in enumerate(axes):
        data = datasets[i]
        data_extracted = datasets_extracted(i)

        if n_burnin > 0:
            data_pre = data[:n_remove]
            data_post = data[n_remove-1:]
            i_pre = iterations[:n_remove]
            i_post = iterations[n_remove-1:]
        else:
            data_pre = []
            data_post = data
            i_pre = []
            i_post = iterations

        i_pre = numpy.asarray(i_pre) + n_skip
        i_post = numpy.asarray(i_post) + n_skip

        # Plot burnin period as shaded region
        if n_burnin > 0:
            ax.axvspan(
                0, n_burnin, color=color_shade,
            )

        # Plot post-burnin chains
        ax.plot(
            i_post, data_post,
            linestyle="solid", color=color_postburnin, alpha=alpha_lines,
        )
        # Plot pre-burnin chains
        ax.plot(
            i_pre, data_pre,
            linestyle="solid", color=color_burnin, alpha=alpha_lines,
        )

        # Plot final kept samples
        if acorr_analyzed:
            for w, it in enumerate(iterations_extracted):
                ax.plot(
                    it, data_extracted[w],
                    marker="o", markersize=2, linestyle="none",
                    color=color_kept, alpha=alpha_points,
                )

    axes[0].set_ylabel(r"$\log p$")

    for i in range(1, n_dim+1):
        axes[i].set_ylabel(r"Param #{i}".format(i=i))

    axes[-1].set_xlabel("Iteration #")

    fig.tight_layout()
    fig.savefig(filename)


def plot_cleaned_chain(
        filename,
        log_prob_extracted, pos_extracted,
    ):
    import numpy
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    n_dim = len(pos_extracted[0][0])
    n_samples = max(len(p) for p in pos_extracted)
    n_walkers = len(pos_extracted)

    n_subplots = n_dim+1

    def datasets_extracted(i):
        if i == 0:
            return log_prob_extracted
        else:
            return [p[:,i-1] for p in pos_extracted]

    alpha = max(0.05, 1.0 / n_dim)

    fig, axes = plt.subplots(
        n_subplots,
        figsize=(8, 4*n_subplots),
        sharex=True,
    )

    for i, ax in enumerate(axes):
        data = datasets_extracted(i)
        # Plot chains
        for w, d in enumerate(data):
            ax.plot(
                d,
                marker="o", markersize=2, linestyle="none",
                color="black", alpha=alpha,
            )

    axes[0].set_ylabel(r"$\log p$")

    for i in range(1, n_dim+1):
        axes[i].set_ylabel(r"Param #{i}".format(i=i))

    axes[-1].set_xlabel("Iteration #")

    fig.tight_layout()
    fig.savefig(filename)


def plot_gelman_rubin(
        filename,
        sqrt_R_stats, n_gelman_rubin,
    ):
    import itertools

    import numpy
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    n_samples, n_dim = numpy.shape(sqrt_R_stats)
    ns = numpy.arange(n_samples)

    sqrt_R_thresh = sqrt_R_stats[n_gelman_rubin]

    fig, ax = plt.subplots(figsize=(8,4))

    plot_formatters = itertools.cycle(
        itertools.product(linestyle_cycle, color_cycle)
    )

    for d, (linestyle, color) in zip(range(n_dim), plot_formatters):
        ax.plot(
            ns, sqrt_R_stats[...,d],
            color=color, linestyle=linestyle,
            label="Param #{i}".format(i=d+1),
        )

    ax.axvline(n_gelman_rubin, color="black")
    ax.axhline(numpy.max(sqrt_R_thresh), color="black")

    ax.set_xlabel("# of samples")
    ax.set_ylabel(r"$\sqrt{\hat{R}}$")

    ax.set_xscale("log")
#    ax.set_yscale("log")

    ax.legend(loc="best")

    fig.tight_layout()
    fig.savefig(filename)


def plot_auto_correlation(
        filename,
        acorrs, acorr_errs,
    ):
    import warnings
    import itertools

    import numpy
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    n_lags, n_walkers, n_dim = numpy.shape(acorrs)
    lags = numpy.arange(n_lags)

    fig, ax = plt.subplots(figsize=(8,4))

    for d, color in zip(range(n_dim), itertools.cycle(color_cycle)):
        ac = acorrs[...,d]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", RuntimeWarning)

            lo = numpy.nanmin(ac, axis=1)
            hi = numpy.nanmax(ac, axis=1)
            med = numpy.nanmedian(ac, axis=1)

        no_nans = ~(numpy.isnan(lo) | numpy.isnan(hi) | numpy.isnan(med))

        ax.fill_between(
            lags[no_nans], lo[no_nans], hi[no_nans],
            color=color, alpha=0.1,
        )
        ax.plot(
            lags[no_nans], med[no_nans],
            color=color,
            label="Param #{i}".format(i=d+1),
        )

    ax.set_xlabel("lag")
    ax.set_ylabel("auto-correlation")

    ax.set_xscale("log")
#    ax.set_yscale("log")

    ax.legend(loc="best")

    fig.tight_layout()
    fig.savefig(filename)


def plot_integrated_auto_correlation(filename, acorr_times, acorr_time_errs):
    import itertools

    import numpy
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    K_final_plus_one, n_dim = numpy.shape(acorr_times)
    K_final = K_final_plus_one - 1
    Ks = numpy.arange(K_final_plus_one)

    fig, ax = plt.subplots(figsize=(8,4))

    for d, color in zip(range(n_dim), itertools.cycle(color_cycle)):
        ac = acorr_times[...,d]
        ax.plot(
            Ks, ac,
            color=color,
            label="Param #{i}".format(i=d+1),
        )

        if acorr_time_errs is not None:
            ac_err = acorr_time_errs[...,d]
            ax.fill_between(
                Ks, ac-ac_err, ac+ac_err,
                color=color,
                alpha=0.1,
            )
            ax.fill_between(
                Ks, ac-2.0*ac_err, ac+2.0*ac_err,
                color=color,
                alpha=0.1,
            )


    ax.set_xlabel(r"$K$")
    ax.set_ylabel(r"$\tau_K$")

    ax.set_xscale("log")
#    ax.set_yscale("log")

    ax.legend(loc="best")

    fig.tight_layout()
    fig.savefig(filename)
